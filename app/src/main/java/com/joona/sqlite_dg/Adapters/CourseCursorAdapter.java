package com.joona.sqlite_dg.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.joona.sqlite_dg.R;

public class CourseCursorAdapter extends CursorAdapter {

    Cursor cursor;
    Context context;

    public CourseCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor);
        this.cursor = cursor;
        this.context = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.course_item_layout, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView CourseName = view.findViewById(R.id.name);
        TextView Holes = view.findViewById(R.id.holes_value);
        TextView Par = view.findViewById(R.id.par_value);
        TextView Length = view.findViewById(R.id.length_value);
        TextView Rounds = view.findViewById(R.id.rounds_value);
        TextView AvScore = view.findViewById(R.id.av_score_value);

        String coursename = cursor.getString(cursor.getColumnIndexOrThrow("Name"));
        int holes = cursor.getInt(cursor.getColumnIndexOrThrow("Holes"));
        int par = cursor.getInt(cursor.getColumnIndexOrThrow("Par"));
        int length = cursor.getInt(cursor.getColumnIndexOrThrow("Length"));
        int rounds = cursor.getInt(cursor.getColumnIndexOrThrow("Rounds"));
        double avscore = cursor.getDouble(cursor.getColumnIndexOrThrow("AvScore"));

        CourseName.setText(coursename);
        Holes.setText(String.valueOf(holes));
        Par.setText(String.valueOf(par));
        Length.setText(String.valueOf(length));
        Rounds.setText(String.valueOf(rounds));
        AvScore.setText(String.format("%.2f", (avscore)).replaceAll(",", "."));
    }

}
