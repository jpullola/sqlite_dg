package com.joona.sqlite_dg.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.joona.sqlite_dg.DatabaseHelper;
import com.joona.sqlite_dg.R;

import java.util.ArrayList;
import java.util.List;

/*
* This is a helper class for editing hole pars and lengths on a selected course
* */

public class EditHoleAdapter extends ArrayAdapter {

    //Initialize variables
    private List<Integer> par, length;
    private Context context;
    private String course;
    DatabaseHelper databaseHelper;

    public EditHoleAdapter(List<Integer> par, List<Integer> length, Context context, String course) {
        super(context, R.layout.edit_hole_layout, par);
        this.context = context;
        this.par = par;
        this.length = length;
        this.course = course;
        databaseHelper = new DatabaseHelper(context);
    }

    @Override
    public int getCount() {
        return par.size();
    }

    @Override
    public List<Integer> getItem(int position){
        List<Integer> holeInfo = new ArrayList<>();
        holeInfo.add(par.get(position));
        holeInfo.add(length.get(position));
        return holeInfo;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View row = inflater.inflate(R.layout.edit_hole_layout, parent, false);

        //Initialize views
        TextView holeNumber = row.findViewById(R.id.edit_hole_num);
        final EditText holeParValue = row.findViewById(R.id.edit_hole_par_value);
        final EditText holeLengthValue = row.findViewById(R.id.edit_hole_length_value);

        //Set values for EditTexts (empty if 0)
        holeNumber.setText(String.valueOf(position + 1));
        if(par.get(position) == 0)
            holeParValue.setText("");
        else
            holeParValue.setText(String.valueOf(par.get(position)));
        if(length.get(position) == 0)
            holeLengthValue.setText("");
        else
            holeLengthValue.setText(String.valueOf(length.get(position)));

        holeParValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                //Write par changes to database
                if(holeParValue.getText().length() > 0){
                    par.set(position, Integer.parseInt(holeParValue.getText().toString()));
                    databaseHelper.changePar(course, par);
                }
            }
        });

        holeLengthValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                //Write length changes to database
                if(holeLengthValue.getText().length() > 0){
                    length.set(position, Integer.parseInt(holeLengthValue.getText().toString()));
                    databaseHelper.changeLength(course, length);
                }
            }
        });

        return row;
    }
}
