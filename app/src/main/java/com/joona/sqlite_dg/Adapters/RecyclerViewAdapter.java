package com.joona.sqlite_dg.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.joona.sqlite_dg.R;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<Integer> holePars;
    private List<Integer> holeScores;
    private Context context;

    public RecyclerViewAdapter(Context context, List<Integer> holePars, List<Integer> holeScores) {
        this.holePars = holePars;
        this.holeScores = holeScores;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.gridview_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.setIsRecyclable(false);

        viewHolder.textView1.setText(String.valueOf(i + 1));
        viewHolder.textView2.setText(String.valueOf(holePars.get(i)));

        if(holeScores.get(i) != 0){
            viewHolder.textView3.setText(String.valueOf(holeScores.get(i)));
            if(holeScores.get(i) == 1)
                viewHolder.textView3.setBackgroundColor(context.getResources().getColor(R.color.color_HIO));
            else if(holeScores.get(i) < holePars.get(i)){
                if(holeScores.get(i) > (holePars.get(i) - 2))
                    viewHolder.textView3.setBackgroundColor(context.getResources().getColor(R.color.color_birdie));
                else if(holeScores.get(i) < (holePars.get(i) - 1))
                    viewHolder.textView3.setBackgroundColor(context.getResources().getColor(R.color.color_eagle));
            }
            else if(holeScores.get(i) == (holePars.get(i) + 1))
                viewHolder.textView3.setBackgroundColor(context.getResources().getColor(R.color.color_bogey));
            else if(holeScores.get(i) == (holePars.get(i) + 2))
                viewHolder.textView3.setBackgroundColor(context.getResources().getColor(R.color.color_double_bogey));
            else if(holeScores.get(i) == (holePars.get(i) + 3))
                viewHolder.textView3.setBackgroundColor(context.getResources().getColor(R.color.color_triple_bogey));
            else if(holeScores.get(i) > (holePars.get(i) + 3))
                viewHolder.textView3.setBackgroundColor(context.getResources().getColor(R.color.color_quad_plus_bogey));
        }
    }

    @Override
    public int getItemCount() {
        return holePars.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView textView1, textView2, textView3;

        public ViewHolder( View itemView) {
            super(itemView);
            textView1 = itemView.findViewById(R.id.grid_textView_1);
            textView2 = itemView.findViewById(R.id.grid_textView_2);
            textView3 = itemView.findViewById(R.id.grid_textView_3);
        }
    }
}
