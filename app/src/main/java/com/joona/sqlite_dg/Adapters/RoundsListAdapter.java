package com.joona.sqlite_dg.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.joona.sqlite_dg.DatabaseHelper;
import com.joona.sqlite_dg.R;

import java.util.List;

public class RoundsListAdapter extends ArrayAdapter {

    private List<Integer> Scores;
    private List<String> RoundNames;
    List<Boolean> finished;
    private Context context;
    private int CoursePar;
    DatabaseHelper databaseHelper;

    public RoundsListAdapter(List<String> RoundNames, List<Integer> Scores, int CoursePar, Context context, List<Boolean> finished) {
        super(context, R.layout.rounds_item_layout, RoundNames);
        this.context = context;
        this.RoundNames = RoundNames;
        this.Scores = Scores;
        this.CoursePar = CoursePar;
        this.finished = finished;
        databaseHelper = new DatabaseHelper(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View row = inflater.inflate(R.layout.rounds_item_layout, parent, false);
        int scoreToPar = Scores.get(position) - CoursePar;

        TextView round = row.findViewById(R.id.round_name_textView);
        TextView score = row.findViewById(R.id.round_score_value_textView);
        TextView scoreParValue = row.findViewById(R.id.round_score_value_to_par_textView);

        round.setText(RoundNames.get(position));
        score.setText(String.valueOf(Scores.get(position)));
        if(finished.get(position)) {
            if (scoreToPar < 0) {
                scoreParValue.setText(String.valueOf(scoreToPar));
            } else if (scoreToPar > 0) {
                scoreParValue.setText("+" + String.valueOf(scoreToPar));
            } else {
                scoreParValue.setText("E");
            }
        } else {
            scoreParValue.setText("-");
        }

        return row;
    }

    public void removeRound(List<String> rounds){
        for(String round : rounds){
            int position = RoundNames.indexOf(round);
            RoundNames.remove(position);
            Scores.remove(position);
            finished.remove(position);
        }
        notifyDataSetChanged();
    }

}
