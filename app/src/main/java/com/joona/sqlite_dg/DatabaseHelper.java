package com.joona.sqlite_dg;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.strictmode.SqliteObjectLeakedViolation;
import android.support.constraint.ConstraintLayout;

import org.apache.http.params.CoreConnectionPNames;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {

    public DatabaseHelper(Context context) {
        super(context, "dgscores", null, 1);
        SQLiteDatabase db = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists Courses (_id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Holes INTEGER, Par INTEGER, Length INTEGER, Rounds INTEGER, AvScore DOUBLE)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean addCourse(String name, int numOfHoles){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        db.execSQL("create table if not exists Courses (_id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Holes INTEGER, Par INTEGER, Length INTEGER, Rounds INTEGER, AvScore DOUBLE)");

        // Check if course already exists
        Cursor cursor = db.rawQuery("SELECT Name FROM Courses", null);
        while(cursor.moveToNext()) {
            if(cursor.getString(cursor.getColumnIndexOrThrow("Name")).equals(name))
                return false;
        }
        cursor.close();

        contentValues.put("Name", name);
        contentValues.put("Holes", numOfHoles);
        contentValues.put("Par", numOfHoles * 3);
        contentValues.put("Length", 0);
        contentValues.put("Rounds", 0);
        contentValues.put("AvScore", 0.0);
        db.insert("Courses", null, contentValues);
        contentValues.clear();

        db.execSQL("create table if not exists [" + name + "] (_id INTEGER PRIMARY KEY AUTOINCREMENT, Hole INTEGER, Par INTEGER, Length INTEGER, AvScore DOUBLE)");
        String roundsFile = "(_id INTEGER PRIMARY KEY AUTOINCREMENT, Time TEXT, ";
        for (int i = 1; i <= numOfHoles; i++) {
            contentValues.put("Hole", i);
            contentValues.put("Par", 3);
            contentValues.put("AvScore", 0.0);
            db.insert("[" + name + "]", null, contentValues);

            roundsFile += "h" + String.valueOf(i) + " INTEGER, ";
        }
        roundsFile += "Total INTEGER, Finished INTEGER)";
        contentValues.clear();

        db.execSQL("create table if not exists [" + name + "_rounds] " + roundsFile);

        return true;
    }

    public void deleteCourse(String name){
        SQLiteDatabase db = getWritableDatabase();

        db.delete("Courses", "Name = ?", new String[] { name });
        db.execSQL("DROP TABLE IF EXISTS [" + name + "]");
        db.execSQL("DROP TABLE IF EXISTS [" + name + "_rounds]");
    }

    public void changePar(String course, List<Integer> newPar){
        SQLiteDatabase db = this.getWritableDatabase();
        int totalPar = 0;
        for (int i = 0; i < newPar.size(); i++) {
            totalPar += newPar.get(i);
            db.execSQL("UPDATE [" + course + "] SET Par = " + newPar.get(i) + " WHERE Hole = ?", new String[] { String.valueOf(i + 1) });
        }
        db.execSQL("UPDATE Courses SET Par = " + totalPar + " WHERE Name = ?", new  String[] { course });
    }

    public void changeLength(String course, List<Integer> newLength){
        SQLiteDatabase db = this.getWritableDatabase();
        int totalLength = 0;
        for (int i = 0; i < newLength.size(); i++) {
            totalLength += newLength.get(i);
            db.execSQL("UPDATE [" + course + "] SET Length = " + newLength.get(i) + " WHERE Hole = ?", new String[] { String.valueOf(i + 1) });
        }
        db.execSQL("UPDATE Courses SET Length = " + totalLength + " WHERE Name = ?", new  String[] { course });
    }

    public boolean changeCourseName(String oldName, String newName){
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT Name FROM Courses", null);
        while(cursor.moveToNext()) {
            if(cursor.getString(cursor.getColumnIndexOrThrow("Name")).equals(newName))
                return false;
        }
        cursor.close();

        ContentValues contentValues = new ContentValues();
        contentValues.put("Name", newName);
        db.update("Courses", contentValues, "Name = ?", new String[] { oldName });

        db.execSQL("ALTER TABLE [" + oldName + "] RENAME TO [" + newName + "]");
        db.execSQL("ALTER TABLE [" + oldName + "_rounds] RENAME TO [" + newName + "_rounds]");

        return true;
    }

    public double calculateCourseAvScore(String course) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select Total, Finished from [" + course + "_rounds]", null);
        int count = 0;
        double sum = 0.0;
        while (cursor.moveToNext()) {
            if (cursor.getInt(cursor.getColumnIndexOrThrow("Finished")) == 1) {
                sum += cursor.getInt(cursor.getColumnIndexOrThrow("Total"));
                count++;
            }
        }
        cursor.close();
        if (count == 0)
            return 0.0;
        return (sum / count);
    }

    public List<Double> getHoleAvScores(String course) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from [" + course + "_rounds]", null);
        List<Double> avScores = new ArrayList<>();
        for (int i = 2; i < cursor.getColumnCount() - 2; i++) {
            double score = 0.0;
            int count = 0;
            cursor.moveToFirst();
            if(cursor.getInt(cursor.getColumnIndexOrThrow("Finished")) == 1) {
                score += cursor.getInt(i);
                count++;
            }
            while(cursor.moveToNext()){
                if(cursor.getInt(cursor.getColumnIndexOrThrow("Finished")) == 1) {
                    score += cursor.getInt(i);
                    count++;
                }
            }
            if(count != 0) {
                avScores.add(score / count);
            } else {
                avScores.add(0.00);
            }
        }
        cursor.close();
        return avScores;
    }

    public List<Integer> getRoundScores(String course, String round){
        SQLiteDatabase db = this.getWritableDatabase();
        List<Integer> scores = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT * FROM [" + course + "_rounds] WHERE Time = ?", new String[] { round });
        cursor.moveToFirst();
        for (int i = 1; i < cursor.getColumnCount() - 3; i++) {
            String column = "h" + i;
            scores.add(cursor.getInt(cursor.getColumnIndexOrThrow(column)));
        }
        cursor.close();
        return scores;
    }

    public int getNumOfHoles(String course){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT Holes FROM COURSES WHERE Name = ?", new String[] { course });
        cursor.moveToFirst();
        int numOfHoles = cursor.getInt(0);
        cursor.close();
        return numOfHoles;
    }

    public int getCoursePar(String course){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select Par from Courses where Name = ?", new String[] { course });
        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    public List<Integer> getPars(String course){
        SQLiteDatabase db = this.getWritableDatabase();
        List<Integer> pars = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT Par FROM [" + course + "]", null);
        while(cursor.moveToNext()){
            pars.add(cursor.getInt(0));
        }
        cursor.close();
        return pars;
    }

    public List<Integer> getLengths(String course){
        SQLiteDatabase db = this.getWritableDatabase();
        List<Integer> lengths = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT Length FROM [" + course + "]", null);
        while(cursor.moveToNext()){
            lengths.add(cursor.getInt(0));
        }
        cursor.close();
        return lengths;
    }

    public List<Double> getAvScores(String course){
        SQLiteDatabase db = this.getWritableDatabase();
        List<Double> avScores = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT AvScore FROM [" + course + "]", null);
        while(cursor.moveToNext()){
            avScores.add(cursor.getDouble(0));
        }
        cursor.close();
        return avScores;
    }

    public List<String> getAllCourses(){
        SQLiteDatabase db = this.getWritableDatabase();
        List<String> courses = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT Name FROM Courses ORDER BY Rounds DESC", null);
        while(cursor.moveToNext()){
            courses.add(cursor.getString(cursor.getColumnIndexOrThrow("Name")));
        }
        cursor.close();
        return courses;
    }

    public List<Integer> getCourseParameterValues(String course, String parameter){
        SQLiteDatabase db = this.getWritableDatabase();
        List<Integer> parameterValues = new ArrayList<>();
        Cursor cursor = db.rawQuery("SELECT " + parameter + " FROM [" + course + "]", null);
        while(cursor.moveToNext()){
            parameterValues.add(cursor.getInt(cursor.getColumnIndexOrThrow(parameter)));
        }
        cursor.close();
        return parameterValues;
    }

    public Cursor readTable(String table, String sortBy, boolean ascending){
        SQLiteDatabase db = this.getWritableDatabase();
        if(sortBy == null){
            return db.rawQuery("select * from [" + table + "]", null);
        } else {
            if(ascending)
                return db.rawQuery("select * from [" + table + "] order by " + sortBy + " asc", null);
            else
                return db.rawQuery("select * from [" + table + "] order by " + sortBy + " desc", null);
        }
    }



    public boolean newRound(String course, String timeStamp){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        Cursor cursor = db.rawQuery("select Time from [" + course + "_rounds] ", null);
        while(cursor.moveToNext()) {
            if(timeStamp.equals(cursor.getString(0)))
                return false;
        }
        cursor.close();

        db.execSQL("UPDATE Courses SET Rounds = " + (getNumOfRounds(course) + 1) + " WHERE Name = ?", new String[] { course });

        contentValues.put("Time", timeStamp);
        cursor = db.rawQuery("select Holes from Courses where Name = ?", new String[] { course });
        cursor.moveToNext();

        contentValues.put("Total", 0);
        contentValues.put("Finished", 0);
        db.insert("[" + course + "_rounds]", null, contentValues);
        cursor.close();
        contentValues.clear();

        return true;
    }

    public void deleteRound(String course, String time){
        SQLiteDatabase db = this.getWritableDatabase();
        int roundCount = getNumOfRounds(course);

        db.execSQL("UPDATE Courses SET Rounds = " + (roundCount - 1) + " WHERE Name = ?", new String[]{course});
        db.delete("[" + course + "_rounds]", "Time = ?", new String[] { time });
    }

    public void updateScores(String course, String round, List<Integer> scores){
        SQLiteDatabase db = this.getWritableDatabase();
        int totalScore = 0;
        for (int i = 1; i <= scores.size(); i++) {
            String column = "h" + i;
            db.execSQL("UPDATE [" + course + "_rounds] SET " + column + " = " + scores.get(i - 1) + " WHERE Time = ?", new String[] { round });
            totalScore += scores.get(i - 1);
        }
        db.execSQL("UPDATE [" + course + "_rounds] SET Total = " + totalScore + " WHERE Time = ?", new String[] { round });
    }

    public int getNumOfRounds(String course){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT Rounds FROM Courses WHERE Name = ?", new String[] { course });
        cursor.moveToFirst();
        int numOfRounds = cursor.getInt(0);
        cursor.close();
        return numOfRounds;
    }

    public int getNumOfFinishedRounds(String course){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT Time FROM [" + course + "_rounds] WHERE Finished = ?", new String[] { "1" });
        return cursor.getCount();
    }

    public List<Integer> getTotalNumOfCoursesAndRounds(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT Rounds FROM Courses", null);
        List<Integer> numOfCoursesAndRounds = new ArrayList<>();
        int numOfRounds = 0;
        int numOfCourses = cursor.getCount();
        while(cursor.moveToNext()){
            numOfRounds += cursor.getInt(0);
        }
        cursor.close();
        numOfCoursesAndRounds.add(numOfCourses);
        numOfCoursesAndRounds.add(numOfRounds);
        return numOfCoursesAndRounds;
    }

    public void setRoundFinishedStatus(String course, String round, boolean isFinished){
        SQLiteDatabase db = this.getWritableDatabase();
        int finished;
        if(isFinished)
            finished = 1;
        else
            finished = 0;
        db.execSQL("UPDATE [" + course + "_rounds] SET Finished = " + finished + " WHERE Time = ?", new String[] { round });
    }

    public void updateAvScores(String course){
        SQLiteDatabase db = this.getWritableDatabase();
        int numOfHoles;
        if(getNumOfRounds(course) > 0) {
            numOfHoles = getNumOfHoles(course);
            List<Double> holeAvScores = getHoleAvScores(course);
            for (int i = 0; i < numOfHoles; i++) {
                db.execSQL("UPDATE [" + course + "] SET AvScore = " + holeAvScores.get(i) + " WHERE Hole = ?", new Integer[]{i + 1});
            }
            double courseAvScore = calculateCourseAvScore(course);
            db.execSQL("UPDATE Courses SET AvScore = " + courseAvScore + " WHERE Name = ?", new String[]{course});
        } else {
            numOfHoles = getNumOfHoles(course);
            for (int i = 0; i < numOfHoles; i++) {
                db.execSQL("UPDATE [" + course + "] SET AvScore = 0.00 WHERE Hole = ?", new Integer[]{i + 1});
            }
            db.execSQL("UPDATE Courses SET AvScore = 0.00 WHERE Name = ?", new String[]{course});
        }
    }

}
