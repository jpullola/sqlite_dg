package com.joona.sqlite_dg.Activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.joona.sqlite_dg.DatabaseHelper;
import com.joona.sqlite_dg.Dialogs.SimpleDialog;
import com.joona.sqlite_dg.R;
import com.joona.sqlite_dg.Adapters.RoundsListAdapter;
import com.joona.sqlite_dg.ScoringFiles.ScoringActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
* This activity is started when the user has selected rounds on a course to be displayed
* This activity displays all played rounds on a selected course on a ListView
*
* Clicking any round starts the scoring activity where the selected round can be edited
* Long clicking any round starts a selection where multiple rounds can be selected and deleted at once
*   All selected rounds are highlighted
*   Toolbar informs how many are selected and delete icon appears on the toolbar*/

public class RoundsListActivity extends AppCompatActivity {

    //Initialize variables
    private Toolbar toolbar;
    private RoundsListAdapter adapter;
    private List<String> rounds = new ArrayList<>();
    private List<Integer> scores = new ArrayList<>();
    private List<Boolean> finished = new ArrayList<>();
    DatabaseHelper databaseHelper;
    String course;
    SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy-HHmmss");
    private String timeStamp;

    public static List<String> SelectedRounds = new ArrayList<>();
    public static boolean isActionMode = false;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        databaseHelper = new DatabaseHelper(this);

        course = getIntent().getStringExtra("Course_Name");
        toolbar = findViewById(R.id.toolBar);
        toolbar.setTitle(course + " - " + getResources().getString(R.string.rounds_2));
        setSupportActionBar(toolbar);

        //Get round info from database and set them in lists
        Cursor cursor = databaseHelper.readTable(course + "_rounds", "_id", false);
        while(cursor.moveToNext()){
            rounds.add(cursor.getString(cursor.getColumnIndexOrThrow("Time")));
            scores.add(cursor.getInt(cursor.getColumnIndexOrThrow("Total")));
            if(cursor.getInt(cursor.getColumnIndexOrThrow("Finished")) == 1)
                finished.add(true);
            else
                finished.add(false);
        }
        cursor.close();

        final ListView listView = findViewById(R.id.mListView);

        listView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(modeListener);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Resume selected round
                //Each time round is started/resumed it is labeled as not finished
                databaseHelper.setRoundFinishedStatus(course, rounds.get(position), false);
                Intent intent = new Intent(RoundsListActivity.this, ScoringActivity.class);
                intent.putExtra("Course_Name", course);
                intent.putExtra("Round_Name", rounds.get(position));
                if(finished.get(position))
                    intent.putExtra("New_Round", false);
                else
                    intent.putExtra("New_Round", true);
                finish();
                startActivity(intent);
            }
        });

        adapter = new RoundsListAdapter(rounds, scores, databaseHelper.getCoursePar(course), this, finished);
        listView.setAdapter(adapter);
    }

    AbsListView.MultiChoiceModeListener modeListener = new AbsListView.MultiChoiceModeListener() {
        //Get all selected rounds and store them in a list

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            if(SelectedRounds.contains(rounds.get(position))) {
                SelectedRounds.remove(rounds.get(position));
            } else {
                SelectedRounds.add(rounds.get(position));
            }
            if(SelectedRounds.size() == 1)
                mode.setTitle(SelectedRounds.size() + " " + getResources().getString(R.string.round_selected_single));
            else
                mode.setTitle(SelectedRounds.size() + " " + getResources().getString(R.string.round_selected_multi));
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            //Initialize action menu
            MenuInflater menuInflater = mode.getMenuInflater();
            menuInflater.inflate(R.menu.contextual_menu, menu);
            isActionMode = true;
            MenuItem itemToHide = menu.findItem(R.id.action_rename);
            itemToHide.setVisible(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            //Handle delete icon click event
            switch (item.getItemId()){
                case R.id.action_delete:
                    for(String round : SelectedRounds){
                        databaseHelper.deleteRound(course, round);
                    }
                    UpdateAvScoresThread thread = new UpdateAvScoresThread(getApplicationContext(), course);
                    thread.start();
                    adapter.removeRound(SelectedRounds);
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            isActionMode = false;
            SelectedRounds.clear();
        }
    };

    class UpdateAvScoresThread extends Thread {
        Context context;
        String course;

        UpdateAvScoresThread(Context context, String course){
            this.context = context;
            this.course = course;
        }

        @Override
        public void run() {
            DatabaseHelper databaseHelper = new DatabaseHelper(context);
            databaseHelper.updateAvScores(course);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Start a new round
        switch (item.getItemId()){
            case R.id.action_add:
                timeStamp = dateFormat.format(new Date());
                databaseHelper.newRound(course, timeStamp);

                Intent intent = new Intent(RoundsListActivity.this, ScoringActivity.class);
                intent.putExtra("Course_Name", course);
                intent.putExtra("Round_Name", timeStamp);
                intent.putExtra("New_Round", true);
                finish();
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
