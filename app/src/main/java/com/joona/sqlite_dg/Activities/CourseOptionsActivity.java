package com.joona.sqlite_dg.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.joona.sqlite_dg.DatabaseHelper;
import com.joona.sqlite_dg.Dialogs.SimpleDialog;
import com.joona.sqlite_dg.R;

/*
* This activity is started when user selects a course to be displayed/modified
* The course can be deleted or renamed using the buttons in the toolbar
*
* Selecting round starts the activity that displays all played rounds on the course
* Selecting holes starts the activity for editing pars and lengths of each hole
* */
public class CourseOptionsActivity extends AppCompatActivity implements SimpleDialog.SimpleDialogListener {

    //Initialize variables
    Toolbar toolbar;
    TextView rounds_textView, holes_textView;
    DatabaseHelper databaseHelper;
    ConstraintLayout holes_listItem, rounds_listItem;
    String course;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_options);
        course = getIntent().getStringExtra("Course_Name");

        toolbar = findViewById(R.id.toolBar);
        toolbar.setTitle(course);
        setSupportActionBar(toolbar);
        databaseHelper = new DatabaseHelper(this);

        //Initialize views
        rounds_textView = findViewById(R.id.course_options_rounds_value);
        holes_textView = findViewById(R.id.course_options_holes_value);
        holes_listItem = findViewById(R.id.course_options_holes);
        rounds_listItem = findViewById(R.id.course_options_rounds);

        setCourseInfo();

        rounds_listItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open activity for displaying all rounds
                final Intent intent = new Intent(CourseOptionsActivity.this, RoundsListActivity.class);
                intent.putExtra("Course_Name", course);
                startActivity(intent);
            }
        });

        holes_listItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open activity for editing hole pars and lengths
                final Intent intent = new Intent(CourseOptionsActivity.this, EditActivity.class);
                intent.putExtra("Course_Name", course);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCourseInfo();
    }

    public void setCourseInfo(){
        //Get num of rounds and holes from database and display them on textViews
        rounds_textView.setText(String.valueOf(databaseHelper.getNumOfRounds(course)));
        holes_textView.setText(String.valueOf(databaseHelper.getNumOfHoles(course)));
    }

    public void openDialog(){
        //Open dialog for asking user to confirm course deletion
        SimpleDialog simpleDialog = new SimpleDialog();

        Bundle bundle = new Bundle();
        bundle.putString("Dialog_Message", getResources().getString(R.string.sure_to_delete) + " " + course + "?");
        bundle.putString("Dialog_Positive_Option", getResources().getString(R.string.delete));
        bundle.putString("Dialog_Negative_Option", getResources().getString(R.string.cancel));

        simpleDialog.setArguments(bundle);
        simpleDialog.show(getSupportFragmentManager(), "Delete");
    }

    public void openRenameCourseDialog(){
        //Open dialog for renaming course
        final Dialog dialog = new Dialog(CourseOptionsActivity.this);
        dialog.setContentView(R.layout.dialog_edit_text);

        Button renameButton_ok = dialog.findViewById(R.id.rename_course_ok_button);
        renameButton_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText newName_editText = dialog.findViewById(R.id.rename_course_editText);
                String newName = newName_editText.getText().toString();
                if(newName.equals("")){
                    Toast.makeText(CourseOptionsActivity.this, getResources().getString(R.string.error_renaming_course), Toast.LENGTH_SHORT).show();
                } else {
                    if(!databaseHelper.changeCourseName(course, newName)){
                        Toast.makeText(CourseOptionsActivity.this, newName + " " + getResources().getString(R.string.already_exists), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(CourseOptionsActivity.this, getResources().getString(R.string.renaming_success), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                        finish();
                    }
                }
            }
        });

        Button renameButton_cancel = dialog.findViewById(R.id.rename_course_cancel_button);
        renameButton_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void passResult(boolean result) {
        if(result){
            databaseHelper.deleteCourse(course);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Create menu with options for deleting and renaming course
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.contextual_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Handle menu item click events
        switch (item.getItemId()){
            case R.id.action_delete:
                openDialog();
                return true;
            case R.id.action_rename:
                openRenameCourseDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
