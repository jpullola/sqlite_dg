package com.joona.sqlite_dg.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import com.joona.sqlite_dg.Adapters.EditHoleAdapter;
import com.joona.sqlite_dg.DatabaseHelper;
import com.joona.sqlite_dg.R;
import java.util.List;

/*
* This activity is started when user chooses to edit holes on a selected course
* This activity is for editing pars and lengths of each hole on a selected course
*
* The par and length values can be changed by entering new values for them using the EditTexts
* */

public class EditActivity extends AppCompatActivity {

    //Initialize variables
    private Toolbar toolbar;
    DatabaseHelper databaseHelper;
    private EditHoleAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list);
        databaseHelper = new DatabaseHelper(this);

        String course = getIntent().getStringExtra("Course_Name");

        toolbar = findViewById(R.id.toolBar);
        toolbar.setTitle(course + " - " + getResources().getString(R.string.edit_holes));
        setSupportActionBar(toolbar);

        //Get pars and lengths from database
        List<Integer> par = databaseHelper.getCourseParameterValues(course, "Par");
        List<Integer> length = databaseHelper.getCourseParameterValues(course, "Length");

        //Set up ListView
        ListView listView = findViewById(R.id.mListView);
        adapter = new EditHoleAdapter(par, length, this, course);
        listView.setAdapter(adapter);


    }

}
