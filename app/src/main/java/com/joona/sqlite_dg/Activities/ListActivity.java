package com.joona.sqlite_dg.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.joona.sqlite_dg.Adapters.CourseCursorAdapter;
import com.joona.sqlite_dg.DatabaseHelper;
import com.joona.sqlite_dg.Dialogs.AddCourseDialog;
import com.joona.sqlite_dg.R;

/*This activity is started when all courses with their information need to be displayed
* All courses are displayed on ListView with their information
* Courses are ordered according to the number of rounds played on them
*
* Toolbar has a button for adding a new course
* Clicking this opens a dialog where new course info can be entered
*
* Clicking any of the listed courses starts an activity for editing them
* */

public class ListActivity extends AppCompatActivity {

    //Initialize variables
    private Toolbar toolbar;
    DatabaseHelper databaseHelper;
    private CourseCursorAdapter adapter;
    AddCourseDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        toolbar = findViewById(R.id.toolBar);
        toolbar.setTitle(getResources().getString(R.string.courses_1));
        setSupportActionBar(toolbar);
        databaseHelper = new DatabaseHelper(this);
        dialog = new AddCourseDialog();

        ListView listView = findViewById(R.id.mListView);
        adapter = new CourseCursorAdapter(this, databaseHelper.readTable("Courses", "Rounds", false));
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView courseName_textView = view.findViewById(R.id.name);
                Intent intent = new Intent(ListActivity.this, CourseOptionsActivity.class);
                intent.putExtra("Course_Name", courseName_textView.getText().toString());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.changeCursor(databaseHelper.readTable("Courses", "Rounds", false));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_add:
                dialog.openDialog(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



}
