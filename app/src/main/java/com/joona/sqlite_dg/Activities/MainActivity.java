package com.joona.sqlite_dg.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.joona.sqlite_dg.Adapters.RecyclerViewAdapter;
import com.joona.sqlite_dg.DatabaseHelper;
import com.joona.sqlite_dg.Dialogs.AddCourseDialog;
import com.joona.sqlite_dg.R;
import com.joona.sqlite_dg.Dialogs.RadioButtonDialog;
import java.util.ArrayList;
import java.util.List;

/*
* This is an app for disc golf score keeping
* With this app the user can track his/her scores
*
* The user can add new courses with as many holes as needed
* All new courses will have a default length of 0 and par 3 on each hole
*
* Each time a round is started it will be given a name that includes the course name and start time
* Rounds can be edited afterwards from courses->course-name->rounds->round-name
*   Scoring layout has a simple UI where score can be adjusted with + and - buttons
*   The layout has hole info (par, length, average score)
*   User can switch between holes by swiping left or right
*   Clicking the top of screen with round info and total scores displays full round scores on a dialog
*
*
*   */

public class MainActivity extends AppCompatActivity {

    //Initialize variables
    private Toolbar toolbar;
    DatabaseHelper databaseHelper;
    Button courses_button, new_round_button, new_course_button;
    TextView courses_textview, rounds_textview;
    private List<Integer> numOfCoursesAndRounds = new ArrayList<>();
    AddCourseDialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        databaseHelper = new DatabaseHelper(this);
        dialog = new AddCourseDialog();

        numOfCoursesAndRounds = databaseHelper.getTotalNumOfCoursesAndRounds();

        courses_button = findViewById(R.id.courses_button);
        new_round_button = findViewById(R.id.new_round_button);
        new_course_button = findViewById(R.id.new_course_button);

        courses_textview = findViewById(R.id.main_courses_value);
        rounds_textview = findViewById(R.id.main_rounds_value);

        courses_textview.setText(String.valueOf(numOfCoursesAndRounds.get(0)));
        rounds_textview.setText(String.valueOf(numOfCoursesAndRounds.get(1)));

        //Start new round -button is not visible if no courses exist
        if(numOfCoursesAndRounds.get(0) == 0)
            new_round_button.setVisibility(View.GONE);
        else
            new_round_button.setVisibility(View.VISIBLE);

        courses_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Start activity that has all courses listed with their information
                startActivity(new Intent(MainActivity.this, ListActivity.class));
            }
        });

        new_round_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Starts dialog that asks the user for a course to start a new round on
                new RadioButtonDialog().show(getSupportFragmentManager(), "Dialog");
            }
        });

        new_course_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Starts dialog where user can enter new course information
                dialog.openDialog(MainActivity.this);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        numOfCoursesAndRounds = databaseHelper.getTotalNumOfCoursesAndRounds();
        courses_textview.setText(String.valueOf(numOfCoursesAndRounds.get(0)));
        rounds_textview.setText(String.valueOf(numOfCoursesAndRounds.get(1)));
        if(numOfCoursesAndRounds.get(0) == 0)
            new_round_button.setVisibility(View.GONE);
        else
            new_round_button.setVisibility(View.VISIBLE);
    }
}
