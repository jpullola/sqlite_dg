package com.joona.sqlite_dg.ScoringFiles;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.joona.sqlite_dg.DatabaseHelper;
import com.joona.sqlite_dg.Dialogs.SummaryDialog;
import com.joona.sqlite_dg.R;

public class ScoringActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private TextView course_name, round_name;
    private int numOfHoles;
    private String course, round;
    ViewPagerAdapter adapter;
    RoundInfo roundInfo;
    ConstraintLayout roundInfoLayout;
    SummaryDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        roundInfo = new RoundInfo();
        dialog = new SummaryDialog();
        setContentView(R.layout.activity_scoring);

        course = getIntent().getStringExtra("Course_Name");
        round = getIntent().getStringExtra("Round_Name");
        boolean newRound = getIntent().getBooleanExtra("New_Round", true);

        course_name = findViewById(R.id.scoring_course);
        round_name = findViewById(R.id.scoring_round_name);

        course_name.setText(course);
        round_name.setText(round);

        roundInfo.setScoresOnCreate(course, round, this, newRound);
        numOfHoles = roundInfo.getNumOfHoles();

        viewPager = findViewById(R.id.scoring_page);
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), course, round, numOfHoles, this);
        viewPager.setAdapter(adapter);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        roundInfoLayout = findViewById(R.id.round_info_constraintLayout);
        roundInfoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.openDialog(ScoringActivity.this);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        UpdateDatabaseThread thread = new UpdateDatabaseThread(this, course, round);
        thread.start();
    }

    class UpdateDatabaseThread extends Thread {
        Context context;
        String course, round;

        UpdateDatabaseThread(Context context, String course, String round){
            this.context = context;
            this.course = course;
            this.round = round;
        }

        @Override
        public void run() {
            RoundInfo roundInfo = new RoundInfo();
            DatabaseHelper databaseHelper = new DatabaseHelper(context);
            boolean isFinished;
            if(roundInfo.getPlayedHoles() == numOfHoles)
                isFinished = true;
            else
                isFinished = false;
            databaseHelper.updateScores(course, round, roundInfo.getScores());
            databaseHelper.setRoundFinishedStatus(course, round, isFinished);
            databaseHelper.updateAvScores(course);
        }
    }

}
