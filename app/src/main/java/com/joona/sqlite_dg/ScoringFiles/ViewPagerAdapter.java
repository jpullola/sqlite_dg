package com.joona.sqlite_dg.ScoringFiles;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.joona.sqlite_dg.R;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private String course;
    private String round;
    private int numOfHoles;
    Context context;

    public ViewPagerAdapter(FragmentManager fm, String course, String round, int numOfHoles, Context context) {
        super(fm);
        this.course = course;
        this.round = round;
        this.numOfHoles = numOfHoles;
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        ScoringFragment scoringFragment = new ScoringFragment();
        position++;
        Bundle bundle = new Bundle();
        bundle.putInt("Current_Hole", position);
        scoringFragment.setArguments(bundle);

        return scoringFragment;
    }

    @Override
    public int getCount() {
        return numOfHoles;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        position++;
        String hole = context.getString(R.string.hole);
        return hole + " " + position;
    }

}
