package com.joona.sqlite_dg.ScoringFiles;

import android.content.Context;
import com.joona.sqlite_dg.DatabaseHelper;
import java.util.ArrayList;
import java.util.List;

public class RoundInfo {

    private static List<Integer> scores = new ArrayList<>();
    private static List<Integer> par = new ArrayList<>();
    private static List<Integer> length = new ArrayList<>();
    private static List<Double> avScores = new ArrayList<>();
    private static int totalScore = 0;
    private static int totalScoreToPar = 0;
    private static int numOfHoles;
    private static int numOfRounds;
    private static int playedHoles;
    private static String course;
    private static String round;

    public void setScoresOnCreate(String course, String round, Context context, boolean newRound){
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        RoundInfo.course = course;
        RoundInfo.round = round;
        scores = databaseHelper.getRoundScores(course, round);
        par = databaseHelper.getPars(course);
        length = databaseHelper.getLengths(course);
        avScores = databaseHelper.getAvScores(course);
        numOfRounds = databaseHelper.getNumOfFinishedRounds(course);
        numOfHoles = scores.size();
        if(!newRound){
            for (int i = 0; i < numOfHoles; i++) {
                avScores.set(i, (((avScores.get(i) * (numOfRounds + 1)) - scores.get(i)) / (numOfRounds)));
            }
        }
        setTotalScore();
        setTotalScoreToPar();
    }

    public List<Integer> getScores(){
        return scores;
    }

    public void setScores(int score, int position){
        scores.set(position, score);
        setTotalScore();
        setTotalScoreToPar();
    }

    public List<Integer> getPar() {
        return par;
    }

    public List<Integer> getLength() {
        return length;
    }

    public List<Double> getAvScores() {
        return avScores;
    }

    public void setTotalScore(){
        totalScore = 0;
        playedHoles = 0;
        for(int item : scores){
            totalScore += item;
            if(item != 0){
                playedHoles++;
            }
        }
    }

    public int getTotalScore() {
        return totalScore;
    }

    public int getNumOfRounds() {
        return numOfRounds;
    }

    public int getNumOfHoles() {
        return numOfHoles;
    }

    public int getPlayedHoles() {
        return playedHoles;
    }

    public void setTotalScoreToPar() {
        totalScoreToPar = 0;
        for (int i = 0; i < numOfHoles; i++) {
            if(scores.get(i) != 0){
                totalScoreToPar += (scores.get(i) - par.get(i));
            }
        }
    }

    public int getTotalScoreToPar(){
        return totalScoreToPar;
    }

    public String getCourse() {
        return course;
    }

    public String getRound() {
        return round;
    }

}
