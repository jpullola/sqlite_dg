package com.joona.sqlite_dg.ScoringFiles;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.joona.sqlite_dg.DatabaseHelper;
import com.joona.sqlite_dg.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScoringFragment extends Fragment{

    private TextView hole_par, hole_length, hole_avScore, hole_score, total_score, total_score_to_par, hole_score_to_par;
    private Button plusButton, minusButton;
    private int currentHole;
    private int holePar;
    private int holeScore;
    private int holeScoreToPar;
    private int numOfRounds;
    private int totalScore;
    private int totalScoreToPar;
    private double avScore;

    RoundInfo roundInfo;


    public ScoringFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_scoring, container, false);
        roundInfo = new RoundInfo();
        currentHole = getArguments().getInt("Current_Hole");

        hole_par = view.findViewById(R.id.scoring_par_value);
        hole_length = view.findViewById(R.id.scoring_length_value);
        hole_avScore = view.findViewById(R.id.scoring_avScore_value);
        hole_score = view.findViewById(R.id.scoring_hole_score_value);
        plusButton = view.findViewById(R.id.plus_button);
        minusButton = view.findViewById(R.id.minus_button);
        total_score = getActivity().findViewById(R.id.scoring_total_score_value);
        total_score_to_par = getActivity().findViewById(R.id.scoring_total_score_to_par_value);
        hole_score_to_par = view.findViewById(R.id.scoring_hole_score_to_par);

        numOfRounds = roundInfo.getNumOfRounds();
        avScore = roundInfo.getAvScores().get(currentHole - 1);
        holePar = roundInfo.getPar().get(currentHole - 1);

        hole_par.setText(String.valueOf(holePar));
        hole_length.setText(String.valueOf(roundInfo.getLength().get(currentHole - 1)));
        hole_avScore.setText(String.valueOf(avScore));

        holeScore = roundInfo.getScores().get(currentHole - 1);

        setScoresToPar();

        UpdateScoresThread thread = new UpdateScoresThread();
        thread.start();

        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holeScore = roundInfo.getScores().get(currentHole - 1);
                holeScore++;
                roundInfo.setScores(holeScore, currentHole - 1);
                setScoresToPar();
            }
        });

        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holeScore = roundInfo.getScores().get(currentHole - 1);
                if(holeScore > 0) {
                    holeScore--;
                    roundInfo.setScores(holeScore, currentHole - 1);
                    setScoresToPar();
                }
            }
        });

        return view;
    }

    public void setScoresToPar(){
        totalScore = roundInfo.getTotalScore();
        total_score.setText(String.valueOf(totalScore));

        totalScoreToPar = roundInfo.getTotalScoreToPar();
        if(totalScoreToPar == 0){
            total_score_to_par.setText("E");
        } else if(totalScoreToPar > 0){
            total_score_to_par.setText("+" + String.valueOf(totalScoreToPar));
        } else {
            total_score_to_par.setText(String.valueOf(totalScoreToPar));
        }

        hole_score.setText(String.valueOf(holeScore));

        holeScoreToPar = holeScore - holePar;
        if(holeScore == 0)
            hole_score_to_par.setText("");
        else if(holeScoreToPar == 0)
            hole_score_to_par.setText("E");
        else if(holeScoreToPar > 0)
            hole_score_to_par.setText("+" + holeScoreToPar);
        else
            hole_score_to_par.setText(String.valueOf(holeScoreToPar));

        if(numOfRounds != 0)
            avScore = (((roundInfo.getAvScores().get(currentHole - 1) * (numOfRounds)) + holeScore) / (numOfRounds + 1));
        else
            avScore = holeScore;
        if(holeScore != 0)
            hole_avScore.setText(String.valueOf(Double.parseDouble(String.format("%.2f", avScore).replaceAll(",", "."))));
        else
            hole_avScore.setText(String.valueOf(Double.parseDouble(String.format("%.2f", roundInfo.getAvScores().get(currentHole - 1)).replaceAll(",", "."))));

    }

    class UpdateScoresThread extends Thread {
        @Override
        public void run() {
            DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
            RoundInfo roundInfo = new RoundInfo();
            databaseHelper.updateScores(roundInfo.getCourse(), roundInfo.getRound(), roundInfo.getScores());
        }
    }

}
