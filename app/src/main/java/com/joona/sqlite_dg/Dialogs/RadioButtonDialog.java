package com.joona.sqlite_dg.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import com.joona.sqlite_dg.DatabaseHelper;
import com.joona.sqlite_dg.R;
import com.joona.sqlite_dg.ScoringFiles.ScoringActivity;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RadioButtonDialog extends DialogFragment {

    private String course;
    private String timeStamp;
    List<String> courses = new ArrayList<>();
    SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy-HHmm");

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        final DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        courses = databaseHelper.getAllCourses();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getResources().getString(R.string.select_course));

        builder.setSingleChoiceItems(courses.toArray(new String[0]), -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                course = courses.get(which);
            }
        });

        builder.setPositiveButton(getResources().getString(R.string.start_round), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(course != null) {
                    timeStamp = dateFormat.format(new Date());
                    databaseHelper.newRound(course, timeStamp);

                    Intent intent = new Intent(getContext(), ScoringActivity.class);
                    intent.putExtra("Course_Name", course);
                    intent.putExtra("Round_Name", timeStamp);
                    intent.putExtra("New_Round", true);
                    startActivity(intent);
                }
            }
        });

        builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

        return builder.create();
    }
}
