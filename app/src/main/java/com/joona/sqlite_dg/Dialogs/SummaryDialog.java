package com.joona.sqlite_dg.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import com.joona.sqlite_dg.Adapters.RecyclerViewAdapter;
import com.joona.sqlite_dg.R;
import com.joona.sqlite_dg.ScoringFiles.RoundInfo;
import java.util.ArrayList;
import java.util.List;

public class SummaryDialog {

    private List<Integer> holePars = new ArrayList<>();
    private List<Integer> holeScores = new ArrayList<>();
    RoundInfo roundInfo;
    Button closeButton;

    public void openDialog(Context context){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_summary);
        roundInfo = new RoundInfo();

        holePars = roundInfo.getPar();
        holeScores = roundInfo.getScores();

        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = dialog.findViewById(R.id.summary_recyclerView);
        recyclerView.setLayoutManager(layoutManager);
        final RecyclerViewAdapter adapter = new RecyclerViewAdapter(context, holePars, holeScores);
        recyclerView.setAdapter(adapter);

        closeButton = dialog.findViewById(R.id.close_summary_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

        Window window = dialog.getWindow();
        if(holePars.size() > 9)
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        else
            window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
    }

}
