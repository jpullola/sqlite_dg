package com.joona.sqlite_dg.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.joona.sqlite_dg.Activities.CourseOptionsActivity;
import com.joona.sqlite_dg.DatabaseHelper;
import com.joona.sqlite_dg.R;

public class AddCourseDialog {

    DatabaseHelper databaseHelper;

    public void openDialog(final Context context){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_add_course);
        databaseHelper = new DatabaseHelper(context);

        final EditText name = dialog.findViewById(R.id.add_course_name_editText);
        final EditText holes = dialog.findViewById(R.id.add_course_holes_editText);

        Button addCourseButton_ok = dialog.findViewById(R.id.add_course_ok_button);
        addCourseButton_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String course_name = name.getText().toString();
                String num_of_holes = holes.getText().toString();
                if(!course_name.equals("") && !num_of_holes.equals("")){
                    if(databaseHelper.addCourse(course_name, Integer.parseInt(num_of_holes))){
                        dialog.dismiss();

                        Intent intent = new Intent(context, CourseOptionsActivity.class);
                        intent.putExtra("Course_Name", course_name);
                        context.startActivity(intent);

                    } else {
                        String errorMessage = course_name + " " + context.getResources().getString(R.string.already_exists);
                        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(context, context.getResources().getString(R.string.error_adding_course), Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button addCourseButton_cancel = dialog.findViewById(R.id.add_course_cancel_button);
        addCourseButton_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}
