package com.joona.sqlite_dg.Dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.widget.Toast;

public class SimpleDialog extends AppCompatDialogFragment {

    private SimpleDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(getArguments().getString("Dialog_Message"));

        builder.setPositiveButton(getArguments().getString("Dialog_Positive_Option"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.passResult(true);
            }
        });

        builder.setNegativeButton(getArguments().getString("Dialog_Negative_Option"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.passResult(false);
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (SimpleDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + "must implement SimpleDialogListener");
        }
    }

    public interface SimpleDialogListener{
        void passResult(boolean result);
    }

}
